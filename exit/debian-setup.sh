apt-get update
apt-get install -y sudo
sudo apt-get install -y tor nyx wget curl fail2ban unbound
sudo systemctl stop tor
cp /etc/resolv.conf /etc/resolv.conf.backup
echo nameserver 127.0.0.1 > /etc/resolv.conf
chattr +i /etc/resolv.conf
wget -O /etc/tor/torrc https://codeberg.org/phin/tor-relays/raw/branch/main/exit/torrc
chown -R debian-tor /etc/tor/torrc
wget -O /var/lib/tor/tor-exit-notice.html https://codeberg.org/phin/tor-relays/raw/branch/main/exit/tor-exit-notice.html
chown -R debian-tor /var/lib/tor
sudo setcap CAP_NET_BIND_SERVICE=+eip /usr/bin/tor
echo "done, now configure /etc/tor/torrc and run sudo systemctl restart tor && sudo systemctl enable tor when done"